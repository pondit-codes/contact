<?php
ob_start();

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
use Pondit\Utility\Debug;


//connecting to database
$dbh = new PDO("mysql:host=localhost;dbname=contact","root","");

//prepare the query
$query = "SELECT * FROM `contacts` WHERE id =".$_GET['id'];

$result = $dbh->query($query);
foreach($result as $row){
    $contact = $row;
}


?>


            <h1>Contact Detail Information</h1>

            <dl>
                <dt>Full Name</dt>
                <dd><?= $contact['firstname']." ".$contact['lastname'];?></dd>

                <dt>Email</dt>
                <dd><?= $contact['email']?></dd>

                <dt>Phone</dt>
                <dd><?= $contact['phone']?></dd>

                <dt>Message</dt>
                <dd><?= $contact['message']?></dd>

            </dl>
<?php
$output = ob_get_contents();
ob_end_clean();

//echo $output;
$layout = file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'Layouts'.DIRECTORY_SEPARATOR.'layout.php');
echo str_replace("||CONTENT||",$output,$layout);