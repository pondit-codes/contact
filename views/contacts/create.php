<?php
ob_start();
?>

            <h1>Contact Form </h1>
            <!-- We're going to place the form here in the next step -->
            <form id="contact-form" method="post" action="store.php" role="form">

                <div class="messages"></div>

                <div class="controls">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="firstname">First Name *</label>
                                <input id="firstname" type="text" name="firstname" class="form-control" placeholder="Please enter your firstname *"  data-error="Firstname is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lastname">Last Name *</label>
                                <input id="lastname" type="text" name="lastname" class="form-control" placeholder="Please enter your lastname *"  data-error="Lastname is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email *</label>
                                <input id="email" type="email" name="email" class="form-control" placeholder="Please enter your email *"  data-error="Valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input id="phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Message *</label>
                                <textarea id="message" name="message" class="form-control" placeholder="Message for me *" rows="4"  data-error="Please,leave us a message."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success btn-send" value="Send & Save message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted"><strong>*</strong> Template is Power by <a href="http://bootstrapious.com" target="_blank">Bootstrapious</a>.</p>
                        </div>
                    </div>
                </div>

            </form>
<?php
$output = ob_get_contents();
ob_end_clean();

//echo $output;
    $layout = file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'Layouts'.DIRECTORY_SEPARATOR.'layout.php');
    echo str_replace("||CONTENT||",$output,$layout);
    ?>
