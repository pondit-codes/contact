<?php
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
use Pondit\Utility\Debug;


//connecting to database
$dbh = new PDO("mysql:host=localhost;dbname=contact","root","");

//prepare the query
$query = "SELECT * FROM `contacts` WHERE id =".$_GET['id'];

$result = $dbh->query($query);
foreach($result as $row){
    $contact = $row;
}

?>


            <h1>Contact Form </h1>
            <!-- We're going to place the form here in the next step -->
            <form id="contact-form" method="post" action="update.php" role="form">
                <input id="id" type="hidden" name="id" class="form-control"  value="<?= $contact['id'];?>">

                <div class="messages"></div>

                <div class="controls">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="firstname">First Name *</label>
                                <input id="firstname" type="text" name="firstname" class="form-control" placeholder="Please enter your firstname *"  data-error="Firstname is required." value="<?= $contact['firstname'];?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="lastname">Last Name *</label>
                                <input id="lastname" type="text" name="lastname" class="form-control" placeholder="Please enter your lastname *"  data-error="Lastname is required." value="<?= $contact['lastname'];?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email *</label>
                                <input id="email" type="email" name="email" class="form-control" placeholder="Please enter your email *"  data-error="Valid email is required." value="<?= $contact['email'];?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input id="phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone" value="<?= $contact['phone'];?>">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Message *</label>
                                <textarea id="message" name="message" class="form-control" placeholder="Message for me *" rows="4"  data-error="Please,leave us a message."><?= $contact['message'];?></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success btn-send" value="Send & Save message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted"><strong>*</strong> Template is Power by <a href="http://bootstrapious.com" target="_blank">Bootstrapious</a>.</p>
                        </div>
                    </div>
                </div>

            </form>

<?php
$output = ob_get_contents();
ob_end_clean();

//echo $output;
$layout = file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'Layouts'.DIRECTORY_SEPARATOR.'layout.php');
echo str_replace("||CONTENT||",$output,$layout);