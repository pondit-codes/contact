<?php
ob_start();

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";

use Pondit\Utility\Debug;


//connecting to database
$dbh = new PDO("mysql:host=localhost;dbname=contact","root","");

//prepare the query
$query = "SELECT * FROM `contacts`";
$result = $dbh->query($query);

?>
<h1>My Contact List </h1>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Full Name</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($result as $row){ ?>
        <tr>
            <th scope="row"><?= $row['id']; ?></th>
            <td><?php echo $row['firstname']." ".$row['lastname']; ?></td>
            <td><?= $row['email']; ?></td>
            <td> <a href="show.php?id=<?=$row['id']; ?>">Show</a>
                | <a href="edit.php?id=<?=$row['id']; ?>">Edit</a>
                | <a href="delete.php?id=<?=$row['id']; ?>" onclick="return confirm('Are you sure you want to delete item?')">Delete</a></td>
        </tr>
    <?php } ?>

    </tbody>
</table>
<?php
$output = ob_get_contents();
ob_end_clean();

//echo $output;
    $layout = file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'Layouts'.DIRECTORY_SEPARATOR.'layout.php');
    echo str_replace("||CONTENT||",$output,$layout);