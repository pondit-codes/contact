<html>
<head>
    <title>Contact Form</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>

</head>
<body>

<div class="container">

    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="/">Home</a>
        <a class="navbar-brand" href="index.php">All Contacts</a>
        <a class="navbar-brand" href="create.php">Create Contact</a>

    </nav>



    <div class="row">

        <div class="col-lg-8 col-lg-offset-2">

            ||CONTENT||
        </div>

    </div>

</div>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>


