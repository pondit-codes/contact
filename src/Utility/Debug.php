<?php
/**
 * Created by PhpStorm.
 * User: Pondit
 * Date: 4/26/2018
 * Time: 5:02 PM
 */

namespace Pondit\Utility;


class Debug
{
    public static function d($vars){

        echo "<pre>";
        print_r($vars);
        echo "</pre>";
    }

    public static function dd($vars){
        self::d($vars);
        die();
        exit();
    }
}