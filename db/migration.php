
CREATE TABLE `contacts` (
`id` int(11) NOT NULL,
`firstname` varchar(50) DEFAULT NULL,
`lastname` varchar(50) DEFAULT NULL,
`email` varchar(50) DEFAULT NULL,
`phone` varchar(15) DEFAULT NULL,
`message` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='All of our contacts information';


ALTER TABLE `contacts`
ADD PRIMARY KEY (`id`);


ALTER TABLE `contacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;